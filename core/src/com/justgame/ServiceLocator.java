package com.justgame;

import com.justgame.managers.MusicAssetManager;
import com.justgame.managers.ScreenManager;
import com.justgame.managers.TexturesAssetManager;

public class ServiceLocator {
	private static ScreenManager screenManager;
	private static MusicAssetManager musicAssetManager;
	private static TexturesAssetManager texturesAssetManager;
	private static AppPreferences preferences;

	public static ScreenManager getScreenManager() {
		return screenManager;
	}

	public static void setScreenManager(ScreenManager newScreenManager) {
		screenManager = newScreenManager;
	}

	public static MusicAssetManager getMusicManager() {
		return musicAssetManager;
	}

	public static void setMusicManager(MusicAssetManager newMusicAssetManager) {
		musicAssetManager = newMusicAssetManager;
	}

	public static TexturesAssetManager getTextureManager() {
		return texturesAssetManager;
	}

	public static void setTextureManager(TexturesAssetManager newTexturesAssetManager) {
		texturesAssetManager = newTexturesAssetManager;
	}

	public static AppPreferences getPreferences() {
		return preferences;
	}

	public static void setPreferences(AppPreferences newAppPreferences) {
		preferences = newAppPreferences;
	}
}
