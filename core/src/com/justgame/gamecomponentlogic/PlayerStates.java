package com.justgame.gamecomponentlogic;

public class PlayerStates {
	static public final String STAND_UP = "STAND_UP";
	static public final String STAND_UP_LEFT = "STAND_UP_LEFT";
	static public final String STAND_UP_RIGHT = "STAND_UP_RIGHT";
	static public final String STAND_DOWN = "STAND_DOWN";
	static public final String STAND_DOWN_LEFT = "STAND_DOWN_LEFT";
	static public final String STAND_DOWN_RIGHT = "STAND_DOWN_RIGHT";
	static public final String STAND_LEFT = "STAND_LEFT";
	static public final String STAND_RIGHT = "STAND_RIGHT";

	static public final String WALK_UP = "WALK_UP";
	static public final String WALK_UP_LEFT = "WALK_UP_LEFT";
	static public final String WALK_UP_RIGHT = "WALK_UP_RIGHT";
	static public final String WALK_DOWN = "WALK_DOWN";
	static public final String WALK_DOWN_LEFT = "WALK_DOWN_LEFT";
	static public final String WALK_DOWN_RIGHT = "WALK_DOWN_RIGHT";
	static public final String WALK_LEFT = "WALK_LEFT";
	static public final String WALK_RIGHT = "WALK_RIGHT";
}
