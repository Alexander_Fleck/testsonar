package com.justgame.gamecomponentlogic.systems;

import com.badlogic.ashley.core.ComponentMapper;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.SortedIteratingSystem;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.justgame.camera.Camera;
import com.justgame.gamecomponentlogic.components.TextureComponent;
import com.justgame.gamecomponentlogic.components.TransformComponent;

import java.util.Comparator;

public class RenderingSystem extends SortedIteratingSystem {
	private SpriteBatch spriteBatch;
	private Array<Entity> renderQueue;
	private Comparator<Entity> comparator = new ZComparator();
	private ComponentMapper<TextureComponent> textureMap;
	private ComponentMapper<TransformComponent> transformMap;
	private OrthographicCamera camera;

	public RenderingSystem(SpriteBatch spriteBatch, Camera camera) {
		super(Family.all(TransformComponent.class, TextureComponent.class).get(), new ZComparator());

		textureMap = ComponentMapper.getFor(TextureComponent.class);
		transformMap = ComponentMapper.getFor(TransformComponent.class);
		renderQueue = new Array<Entity>();
		this.spriteBatch = spriteBatch;

		this.camera = camera;
	}

	@Override
	public void update(float deltaTime) {
		super.update(deltaTime);

		renderQueue.sort(comparator);

		spriteBatch.setProjectionMatrix(camera.combined);
		spriteBatch.enableBlending();

		spriteBatch.begin();

		for (Entity entity : renderQueue) {
			TextureComponent texture = textureMap.get(entity);
			TransformComponent transform = transformMap.get(entity);

			if (texture.region == null || transform.isHidden) {
				continue;
			}

			float width = texture.region.getRegionWidth();
			float height = texture.region.getRegionHeight();
			float originX = width / 2f;
			float originY = height / 2f;

			spriteBatch.draw(texture.region,
			                 transform.position.x - originX, transform.position.y - originY,
			                 originX, originY,
			                 width, height,
			                 transform.scale.x, transform.scale.y,
			                 transform.rotation);
		}

		spriteBatch.end();

		renderQueue.clear();

		camera.update();
	}

	@Override
	public void processEntity(Entity entity, float deltaTime) {
		renderQueue.add(entity);
	}
}
