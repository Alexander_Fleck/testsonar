package com.justgame.gamecomponentlogic;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.utils.Array;

public class Animations {
	static public Array<AnimationNode> playerAnimations = new Array<AnimationNode>();

	static {
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_UP,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_UP,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_UP_LEFT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_UP_LEFT,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_UP_RIGHT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_UP_RIGHT,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_DOWN,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_DOWN,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_DOWN_LEFT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_DOWN_LEFT,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_DOWN_RIGHT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_DOWN_RIGHT,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_LEFT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_LEFT,
				                     1, 4,
				                     Animation.PlayMode.LOOP

		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.STAND_RIGHT,
				                     1f / 4f,
				                     TextureConstants.Farmer.STAND_RIGHT,
				                     1, 4,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_UP,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_UP,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_UP_LEFT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_UP_LEFT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_UP_RIGHT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_UP_RIGHT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_DOWN,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_DOWN,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_DOWN_LEFT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_DOWN_LEFT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_DOWN_RIGHT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_DOWN_RIGHT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_LEFT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_LEFT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
		playerAnimations.add(new AnimationNode(
				                     PlayerStates.WALK_RIGHT,
				                     1f / 15f,
				                     TextureConstants.Farmer.WALK_RIGHT,
				                     1, 15,
				                     Animation.PlayMode.LOOP
		                     )
		);
	}

	public static class AnimationNode {
		public String state;
		public float frameDuration;
		public String texturePath;
		public int rows;
		public int cols;
		public Animation.PlayMode mode;

		public AnimationNode(String state,
		                     float frameDuration,
		                     String texturePath,
		                     int rows, int cols,
		                     Animation.PlayMode mode) {
			this.state = state;
			this.frameDuration = frameDuration;
			this.texturePath = texturePath;
			this.rows = rows;
			this.cols = cols;
			this.mode = mode;
		}
	}
}
