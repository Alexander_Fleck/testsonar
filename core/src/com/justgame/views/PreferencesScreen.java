package com.justgame.views;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Event;
import com.badlogic.gdx.scenes.scene2d.EventListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.justgame.AppPreferences;
import com.justgame.ServiceLocator;
import com.justgame.managers.MusicAssetManager;
import com.justgame.managers.TexturesAssetManager;

public class PreferencesScreen implements Screen {
	private Stage stage;
	private Skin skin;
	private Label titleLabel;
	private Label volumeMusicLabel;
	private Label volumeSoundLabel;
	private Label musicOnOffLabel;
	private Label soundOnOffLabel;

	public PreferencesScreen() {
		stage = new Stage(new ScreenViewport());
	}

	@Override
	public void show() {
		stage.clear();
		Gdx.input.setInputProcessor(stage);
		ServiceLocator.getMusicManager().playMusic(MusicAssetManager.caveSong);

		final AppPreferences preferences = ServiceLocator.getPreferences();
		Table table = new Table();
		table.setFillParent(true);
		stage.addActor(table);

		skin = ServiceLocator.getTextureManager().getSkin(TexturesAssetManager.SKIN_JSON);

		final Slider volumeMusicSlider = new Slider(0f, 1f, 0.1f, false, skin);
		volumeMusicSlider.setValue(preferences.getMusicVolume());
		volumeMusicSlider.addListener(new EventListener() {
			@Override
			public boolean handle(Event event) {
				preferences.setMusicVolume(volumeMusicSlider.getValue());
				return false;
			}
		});

		final CheckBox musicCheckbox = new CheckBox(null, skin);
		musicCheckbox.setChecked(preferences.isMusicEnabled());
		musicCheckbox.addListener(new EventListener() {
			@Override
			public boolean handle(Event event) {
				boolean enabled = musicCheckbox.isChecked();
				preferences.setMusicEnabled(enabled);
				return false;
			}
		});

		final Slider soundMusicSlider = new Slider(0f, 1f, 0.1f, false, skin);
		soundMusicSlider.setValue(preferences.getSoundVolume());
		soundMusicSlider.addListener(new EventListener() {
			@Override
			public boolean handle(Event event) {
				preferences.setSoundVolume(soundMusicSlider.getValue());
				return false;
			}
		});

		final CheckBox soundEffectsCheckbox = new CheckBox(null, skin);
		soundEffectsCheckbox.setChecked(preferences.isSoundEffectsEnabled());
		soundEffectsCheckbox.addListener(new EventListener() {
			@Override
			public boolean handle(Event event) {
				boolean enabled = soundEffectsCheckbox.isChecked();
				preferences.setSoundEffectsEnabled(enabled);
				return false;
			}
		});

		final TextButton backButton = new TextButton("Back", skin);
		backButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				ServiceLocator.getScreenManager().changeScreen(ViewScreen.MENU);
			}
		});

		final TextButton applyButton = new TextButton("Apply", skin);
		applyButton.addListener(new ChangeListener() {
			@Override
			public void changed(ChangeEvent event, Actor actor) {
				ServiceLocator.getScreenManager().changeScreen(ViewScreen.PREFERENCES);
			}
		});

		titleLabel = new Label("Preferences", skin);
		volumeMusicLabel = new Label("Music Volume", skin);
		volumeSoundLabel = new Label("Sound Volume", skin);
		musicOnOffLabel = new Label("Music", skin);
		soundOnOffLabel = new Label("Sound Effect", skin);

		table.add(titleLabel).colspan(4);
		table.row().colspan(2).pad(10, 0, 0, 10);
		table.add(volumeMusicLabel);
		table.add(volumeMusicSlider);
		table.row().colspan(2).pad(10, 0, 0, 10);
		table.add(musicOnOffLabel);
		table.add(musicCheckbox);
		table.row().colspan(2).pad(10, 0, 0, 10);
		table.add(volumeSoundLabel);
		table.add(soundMusicSlider);
		table.row().colspan(2).pad(10, 0, 0, 10);
		table.add(soundOnOffLabel);
		table.add(soundEffectsCheckbox);
		table.row().colspan(2).pad(10, 0, 0, 10);
		table.add(backButton);
		table.add(applyButton);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub
	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}
}
