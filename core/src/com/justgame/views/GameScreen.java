package com.justgame.views;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.PooledEngine;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.justgame.UI.StatusUI;
import com.justgame.camera.Camera;
import com.justgame.gamecomponentlogic.Animations;
import com.justgame.gamecomponentlogic.PlayerInputController;
import com.justgame.gamecomponentlogic.PlayerStates;
import com.justgame.gamecomponentlogic.components.AnimationComponent;
import com.justgame.gamecomponentlogic.components.StatsComponent;
import com.justgame.gamecomponentlogic.components.TextureComponent;
import com.justgame.gamecomponentlogic.components.TransformComponent;
import com.justgame.gamecomponentlogic.systems.AnimationSystem;
import com.justgame.gamecomponentlogic.systems.PlayerMovement;
import com.justgame.gamecomponentlogic.systems.RenderingSystem;
import com.justgame.map.Map;
import com.justgame.map.MapInputController;

public class GameScreen implements Screen {
	private Camera camera;
	private Map map;
	private MapInputController mapInputController;
	private PlayerInputController playerInputController;
	private InputMultiplexer inputMultiplexer;
	private Stage stage;
	private PooledEngine engine;
	private SpriteBatch spriteBatch = new SpriteBatch();
	private StatsComponent stats = new StatsComponent();
	private StatusUI status_ui;

	public GameScreen() {
		camera = new Camera();
		map = new Map(camera);
		mapInputController = new MapInputController(camera);
		playerInputController = new PlayerInputController();
		stage = new Stage(new ScreenViewport());
		inputMultiplexer = new InputMultiplexer();
		engine = new PooledEngine();

		inputMultiplexer.addProcessor(mapInputController);
		inputMultiplexer.addProcessor(playerInputController);
	}

	@Override
	public void show() {
		Gdx.input.setInputProcessor(inputMultiplexer);

		stage.clear();
		status_ui = new StatusUI();
		stats.addPropertyChangeListener(status_ui);
		status_ui.setVisible(true);
		status_ui.setPosition(0, 0);
		status_ui.setKeepWithinStage(false);
		status_ui.setMovable(false);
		stage.addActor(status_ui);

		map.show();

		init();
	}

	private void init() {
		Entity player = createPlayer();
		engine.addEntity(player);

		engine.addSystem(new AnimationSystem());
		engine.addSystem(new RenderingSystem(spriteBatch, camera));
		engine.addSystem(new PlayerMovement(player, playerInputController, camera));

		Vector3 cameraPosition = player.getComponent(TransformComponent.class).position;
		cameraPosition.z = 0;
		camera.setCameraPosition(cameraPosition);
	}

	private Entity createPlayer() {
		Entity player = new Entity();

		player.add(StatsComponent.create());
		player.add(TextureComponent.create());

		player.add(TransformComponent.create()
				           .setPosition(new Vector3(10, 10, 1))
				           .setScale(new Vector2(0.5f, 0.5f)));

		AnimationComponent animations = AnimationComponent.create();
		for (Animations.AnimationNode animationNode : Animations.playerAnimations) {
			animations.addAnimation(animationNode);
		}
		animations.setState(PlayerStates.STAND_DOWN);
		player.add(animations);

		return player;
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0f, 0f, 0f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		map.render(delta);
		engine.update(delta);
		stage.act(delta);
		stage.draw();
	}

	@Override
	public void resize(int width, int height) {
		map.resize(width, height);
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		map.hide();
	}

	@Override
	public void dispose() {
		map.dispose();
		stats.removePropertyChangeListener(status_ui);
	}
}
