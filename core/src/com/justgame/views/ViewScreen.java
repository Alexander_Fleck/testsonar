package com.justgame.views;

public enum ViewScreen {
	MENU,
	PREFERENCES,
	GAME,
	ENDGAME,
	LOADING
}
