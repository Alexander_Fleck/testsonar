package com.justgame.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.renderers.IsometricTiledMapRenderer;
import com.badlogic.gdx.math.Vector3;
import com.justgame.ServiceLocator;
import com.justgame.camera.Camera;
import com.justgame.managers.TexturesAssetManager;

public class Map implements Screen {
	private TiledMap map;
	private Camera camera;
	private IsometricTiledMapRenderer renderer;

	public Map(Camera camera) {
		this.camera = camera;
	}

	@Override
	public void show() {
		map = ServiceLocator.getTextureManager().getMap(TexturesAssetManager.MAP);
		renderer = new IsometricTiledMapRenderer(map);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		renderer.setView(camera);
		renderer.render();
	}

	@Override
	public void resize(int width, int height) {
		Vector3 cameraPosition = new Vector3(camera.position);

		camera.setToOrtho(false);
		camera.position.set(cameraPosition);
		camera.update();
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub
	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void dispose() {
		renderer.dispose();
		map.dispose();
	}
}
