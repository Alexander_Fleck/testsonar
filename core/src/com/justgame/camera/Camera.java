package com.justgame.camera;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector3;

public class Camera extends OrthographicCamera {
	private static final float MIN_ZOOM = 0.4f;
	private static final float MAX_ZOOM = 1.6f;

	public void translateCameraPosition(Vector3 position) {
		translate(position);
		update();
	}

	public void setCameraPosition(Vector3 position) {
		this.position.set(position);
		update();
	}

	public boolean changeCameraZoom(float deltaZoom) {
		float newZoom = zoom + deltaZoom;

		if (MIN_ZOOM < newZoom && newZoom < MAX_ZOOM) {
			zoom = newZoom;
			update();

			return true;
		} else {
			return false;
		}
	}
}
