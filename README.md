# rpg-java-game

## Build and launch project

1. Install Java (any, starting from 8). Look at [OpenJDK](https://openjdk.java.net/install/index.html) home page.

2. Clone repository: `git clone https://gitlab.com/tsu-trpo/rpg-java-game.git`.

3. Go to the project folder: `cd rpg-java-game/`.

4. Build project: `./gradlew clean build`.

5. Complete, we can run the game: `./gradlew run`.

The game uses the [libGDX](https://libgdx.badlogicgames.com/) engine.
